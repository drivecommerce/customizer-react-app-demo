# Customizer React Demo - Drive Commerce

This demo loads a Customizer widget inside a React Application. The React framework is NextJS, and the Customizer widget is written in Vue.

## Getting Started

```bash
yarn install
# then
yarn dev
# to start demo
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

## Documentation
To get started with modifying your Customizer project, visit the [Customizer Documentation Site](https://drivecommerce.com/customizer/developer/).