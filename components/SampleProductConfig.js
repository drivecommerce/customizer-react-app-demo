import productConfig from './SP14-SOLID.json'
const SampleProductConfig = () => {
    return (
        <textarea
            className="js-variants-sp14-solid"
            rows="10"
            cols="80"
            style={{'display': 'none'}}
        >
            {JSON.stringify(productConfig)}
        </textarea>
    )
}

export default SampleProductConfig;