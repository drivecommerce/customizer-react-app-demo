import { useEffect, memo } from "react";
import initCustomizer from './initCustomizer';
import loadScriptAsync from './loadScriptAsync';

// eslint-disable-next-line react/display-name
const CustomizerContainer = memo(() => {
    // Load Customizer once component has rendered
    useEffect(() => {
        const vendorScriptSource = '//cz.drrv.co/demo/vendors~main.abd2324a.js';
        const widgetSource = '//cz.drrv.co/demo/customize-widget.js'

        // Load client scripts
        loadScriptAsync(vendorScriptSource).then(() => {
            return loadScriptAsync(widgetSource)
        }).then(() => {
            // Customizer is now ready for use.
            initCustomizer();
        });
    }, [])

    return (
        <div id="customize__placeholder">
            Loading Customizer
        </div>
    )
});

export default CustomizerContainer;