const initCustomizer = () => {
    // Check for customizer
    if(!window.CustomizeWidget) {
        return;
    }

    CustomizeWidget.default.createWidget({
        product: "WATCH360",
        apiKey: "62SH6h0VIbRh4kHuEkjbHiErBBQGwOmZfYfYd6miqbiABsWrgi4AhfCStHnbNUawwV8wQ2q2foWHVtaHXd5vYFIno0nxN0cT",
        container: document.querySelector("#customize__placeholder"),
        on: {
            addToCart: function (e) {
                alert("Recipe saved as " + e.id + " at " + e.location);
            },
        },
    });
}

export default initCustomizer;
