// Modified from https://gist.github.com/james2doyle/28a59f8692cec6f334773007b31a1523
const loadScriptAsync = (src) => {
    return new Promise((resolve, reject) => {
        let r = false;
        const s = document.createElement('script');
        s.type = 'text/javascript';
        s.src = src;
        s.onerror = (err) => { reject(err, s); };
        s.onload = s.onreadystatechange = function() {
            if (!r && (!this.readyState || this.readyState == 'complete')) {
                r = true;
                resolve();
            }
        };
        document.body.appendChild(s);
    });
}

export default loadScriptAsync;