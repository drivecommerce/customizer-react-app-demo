/* eslint-disable @next/next/no-sync-scripts */
import Head from 'next/head'

// Customizer container component
import CustomizerContainer from '../components/CustomizerContainer'

export default function Home() {
  return (
    <div>
        <Head>
            <title>Customizer React Demo App</title>

            {/* Necessary front-end scripts */}
            {/* <script type="text/javascript" src="//cz.drrv.co/demo/vendors~main.abd2324a.js"></script>
            <script lang="application/javascript" src="//cz.drrv.co/demo/customize-widget.js"></script> */}
        </Head>

        {/* Customizer Container */}
        <CustomizerContainer />
    </div>
  )
}
